import type { Actions } from './$types';
import { fail } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const validate = (data: FormData) => {
    const criterion = data.get('criterion')?.toString() ?? '';
    const percent = parseFloat(data.get('percent')?.toString() ?? '');

    if (criterion.trim().length === 0) {
        throw Error('Criterion is required.', { cause: 'criterion' });
    }
    
    if (percent <= 0) {
        throw Error('Percent should be greater than zero.', { cause: 'percent' });
    }
};

export const actions = {
    save: async ({ request }) => {
        const formData = await request.formData();
        const data = {
            criterion: formData.get('criterion')?.toString() ?? '',
            percent: parseFloat(formData.get('percent')?.toString() ?? ''),
            weight: parseInt(formData.get('weight')?.toString() ?? '0'),
            hidden: parseInt(formData.get('hidden')?.toString() ?? '0') === 1,
        };
        try {
            validate(formData);
            await prisma.criterion.create({
                data
            });
        } catch (error) {
            if (error instanceof Error) 
                return fail(422, { error: error.message, cause: error.cause, ...data });
        }
        return {
            success: `criterion ${data.criterion} has been created.`
        };
    }
} satisfies Actions;