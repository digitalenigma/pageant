import { PrismaClient } from '@prisma/client';
import type { PageServerLoad } from './$types';

const prisma = new PrismaClient();

export const load = (async () => {
    const criteria = await prisma.criterion.findMany({
        orderBy: [
            { weight: 'asc'},
            { criterion: 'asc' }
        ]
    });
    return {
        criteria
    };
}) satisfies PageServerLoad;