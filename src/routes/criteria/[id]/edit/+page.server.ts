import type { Actions } from './$types';
import { error, fail, redirect } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';
import type { PageServerLoad } from '../$types';

const prisma = new PrismaClient();

export const load = (async ({ params }) => {
    try {
        const criterion = await prisma.criterion.findFirstOrThrow({
            where: {
                id: parseInt(params.id)
            }
        })
        return { criterion };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;

const validate = (data: FormData) => {
    const criterion = data.get('criterion')?.toString() ?? '';
    const percent = parseFloat(data.get('percent')?.toString() ?? '');

    if (criterion.trim().length === 0) {
        throw Error('Criterion is required.', { cause: 'criterion' });
    }
    
    if (percent <= 0) {
        throw Error('Percent should be greater than zero.', { cause: 'percent' });
    }
};

export const actions = {
    save: async ({ request, params }) => {
        const formData = await request.formData();
        const data = {
            criterion: formData.get('criterion')?.toString() ?? '',
            percent: parseFloat(formData.get('percent')?.toString() ?? ''),
            weight: parseFloat(formData.get('weight')?.toString() ?? '0'),
            hidden: parseInt(formData.get('hidden')?.toString() ?? '0') === 1,
        };
        try {
            validate(formData);
            await prisma.criterion.update({
                where: {
                    id: parseInt(params.id)
                },
                data
            });
        } catch (error) {
            if (error instanceof Error) 
                return fail(422, { error: error.message, cause: error.cause, ...data });
        }
        throw redirect(303, '/criteria');
    }
} satisfies Actions;