import { PrismaClient } from '@prisma/client';
import type { PageServerLoad } from './$types';
import { error } from '@sveltejs/kit';

const prisma = new PrismaClient();

export const load = (async ({ params }) => {
    try {
        const criterion = await prisma.criterion.findFirstOrThrow({
            where: {
                id: parseInt(params.id)
            }
        })
        return { criterion };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;