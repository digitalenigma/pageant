import { PrismaClient } from '@prisma/client';
import type { PageServerLoad } from './$types';

const prisma = new PrismaClient();

export const load = (async () => {
    const roles = await prisma.role.findMany();
    return {
        roles
    };
}) satisfies PageServerLoad;