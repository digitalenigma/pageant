import type { Actions } from './$types';
import { fail } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const validate = (data: FormData) => {
    const role = data.get('role')?.toString() ?? '';
    const permissions = data.get('permissions')?.toString() ?? '';

    if (role.trim().length === 0) {
        throw Error('Role is required.', { cause: 'role' });
    }
    
    if (permissions.trim().length === 0) {
        throw Error('Permissions is required.', { cause: 'permissions' });
    }
};

export const actions = {
    save: async ({ request }) => {
        const formData = await request.formData();
        const data = {
            role: formData.get('role')?.toString() ?? '',
            permissions: formData.get('permissions')?.toString() ?? ''
        };
        try {
            validate(formData);
            await prisma.role.create({
                data
            });
        } catch (error) {
            if (error instanceof Error) 
                return fail(422, { error: error.message, cause: error.cause, ...data });
        }
        return {
            success: `Role ${data.role} has been created.`
        };
    }
} satisfies Actions;