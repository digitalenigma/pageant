import { PrismaClient } from '@prisma/client';
import type { PageServerLoad } from './$types';
import { error } from '@sveltejs/kit';

const prisma = new PrismaClient();

export const load = (async ({ params }) => {
    try {
        const role = await prisma.role.findFirstOrThrow({
            where: {
                id: parseInt(params.id)
            }
        })
        return { role };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;