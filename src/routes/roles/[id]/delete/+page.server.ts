import type { Actions, PageServerLoad } from './$types';
import { fail, error, redirect } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const load = (async ({ params }) => {
    try {
        const role = await prisma.role.findFirstOrThrow({
            where: {
                id: parseInt(params.id)
            }
        })
        return { role };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;

export const actions = {
    delete: async ({ params }) => {
        try {
            await prisma.role.delete({
                where: {
                    id: parseInt(params.id)
                }
            });
        } catch (error) {
            if (error instanceof Error) 
                return fail(422, { error: error.message });
        }
        throw redirect(303, '/roles');
    }
} satisfies Actions;