import type { Actions, PageServerLoad } from './$types';
import { fail, error } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const validate = (data: FormData) => {
    const role = data.get('role')?.toString() ?? '';
    const permissions = data.get('permissions')?.toString() ?? '';

    if (role.trim().length === 0) {
        throw Error('Role is required.', { cause: 'role' });
    }
    
    if (permissions.trim().length === 0) {
        throw Error('Permissions is required.', { cause: 'permissions' });
    }
};

export const load = (async ({ params }) => {
    try {
        const role = await prisma.role.findFirstOrThrow({
            where: {
                id: parseInt(params.id)
            }
        })
        return { role };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;

export const actions = {
    save: async ({ request, params }) => {
        const formData = await request.formData();
        const data = {
            role: formData.get('role')?.toString() ?? '',
            permissions:  formData.get('permissions')?.toString() ?? ''
        };
        try {
            validate(formData);
            await prisma.role.update({
                data,
                where: {
                    id: parseInt(params.id)
                }
            });
        } catch (error) {
            if (error instanceof Error) 
                return fail(422, { error: error.message, cause: error.cause, ...data });
        }
        return {
            success: `Role ${data.role} has been updated.`
        };
    }
} satisfies Actions;