import { PrismaClient } from '@prisma/client';
import type { LayoutServerLoad } from './$types';
import type { Cookies } from '@sveltejs/kit';

const prisma = new PrismaClient();

export const load = (async ({cookies, url}) => {
    const title = await getTitle();
    const user = await getUser(cookies);
    const categories = await prisma.category.findMany({
        orderBy: [
            { weight: 'asc' },
            { category: 'asc' }
        ]
    });
    return {
        title,
        user,
        categories,
        path: url.pathname
    };
}) satisfies LayoutServerLoad;

const getTitle = (async () => {
    const title = await prisma.setting.findFirst({
        where: {
            name: 'title'
        }
    });
    return title ? title.value : 'Pageant';
});

const getUser = (async (cookies: Cookies) => {
    let user: false | null | string = false;
    const sessionId = cookies.get('session');
    let userSession = undefined;
    if (sessionId) {
        userSession = await prisma.userSession.findFirst({
            where: {
                sessionId
            },
            include: {
                user: true
            }
        });
        if (!userSession) {
            cookies.delete('session');
        } else {
            user = userSession.user.name ?? userSession.user.email;
        }
    }
    return user;
})