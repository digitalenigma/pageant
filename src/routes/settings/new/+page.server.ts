import type { Actions } from './$types';
import { fail } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const validate = (data: FormData) => {
    const name = data.get('name')?.toString() ?? '';
    const value = data.get('value')?.toString() ?? '';

    if (name.trim().length === 0) {
        throw Error('Name is required.', { cause: 'name' });
    }
    
    if (value.trim().length === 0) {
        throw Error('value is required.', { cause: 'value' });
    }
};

export const actions = {
    save: async ({ request }) => {
        const formData = await request.formData();
        const data = {
            name: formData.get('name')?.toString() ?? '',
            value: formData.get('value')?.toString() ?? ''
        };
        try {
            validate(formData);
            await prisma.setting.create({
                data
            });
        } catch (error) {
            if (error instanceof Error) 
                return fail(422, { error: error.message, cause: error.cause, ...data });
        }
        return {
            success: `setting ${data.name} has been created.`
        };
    }
} satisfies Actions;