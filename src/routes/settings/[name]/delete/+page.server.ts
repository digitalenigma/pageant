import type { Actions, PageServerLoad } from './$types';
import { fail, error, redirect } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const load = (async ({ params }) => {
    try {
        const setting = await prisma.setting.findFirstOrThrow({
            where: {
                name: params.name
            }
        })
        return { setting };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;

export const actions = {
    delete: async ({ params }) => {
        try {
            await prisma.setting.delete({
                where: {
                    name: params.name
                }
            });
        } catch (error) {
            if (error instanceof Error) 
                return fail(422, { error: error.message });
        }
        throw redirect(303, '/settings');
    }
} satisfies Actions;