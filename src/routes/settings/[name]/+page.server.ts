import { PrismaClient } from '@prisma/client';
import type { PageServerLoad } from './$types';
import { error } from '@sveltejs/kit';

const prisma = new PrismaClient();

export const load = (async ({ params }) => {
    try {
        const setting = await prisma.setting.findFirstOrThrow({
            where: {
                name: params.name
            }
        })
        return { setting };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;