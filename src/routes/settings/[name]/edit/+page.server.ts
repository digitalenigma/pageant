import type { Actions, PageServerLoad } from './$types';
import { fail, error, redirect } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const validate = (data: FormData) => {
    const name = data.get('name')?.toString() ?? '';
    const value = data.get('value')?.toString() ?? '';

    if (name.trim().length === 0) {
        throw Error('Name is required.', { cause: 'name' });
    }
    
    if (value.trim().length === 0) {
        throw Error('Value is required.', { cause: 'value' });
    }
};

export const load = (async ({ params }) => {
    try {
        const setting = await prisma.setting.findFirstOrThrow({
            where: {
                name: params.name
            }
        })
        return { setting };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;

export const actions = {
    save: async ({ request, params }) => {
        const formData = await request.formData();
        const data = {
            name: formData.get('name')?.toString() ?? '',
            value:  formData.get('value')?.toString() ?? ''
        };
        try {
            validate(formData);
            await prisma.setting.update({
                data,
                where: {
                    name: params.name
                }
            });
        } catch (error) {
            if (error instanceof Error) 
                return fail(422, { error: error.message, cause: error.cause, ...data });
        }
        throw redirect(303, '/settings');
    }
} satisfies Actions;