import { PrismaClient } from '@prisma/client';
import type { PageServerLoad } from './$types';

const prisma = new PrismaClient();

export const load = (async () => {
    const settings = await prisma.setting.findMany();
    return {
        settings
    };
}) satisfies PageServerLoad;