import { PrismaClient } from '@prisma/client';
import type { PageServerLoad } from './$types';
import { error } from '@sveltejs/kit';

const prisma = new PrismaClient();

export const load = (async ({ params }) => {
    try {
        const user = await prisma.user.findFirstOrThrow({
            where: {
                id: parseInt(params.id)
            },
            select: {
                id: true,
                email: true,
                name: true,
                createdAt: true,
                updatedAt: true
            }
        })
        return { user };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;