import type { Actions, PageServerLoad } from './$types';
import { fail, error, redirect } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';
import SecurePassword from 'secure-password';

const prisma = new PrismaClient();
const pwd = new SecurePassword();

const validate = (data: FormData) => {
    const email = data.get('email')?.toString() ?? '';
    const password = data.get('password')?.toString() ?? '';
    const password2 = data.get('password2')?.toString() ?? '';
    const emailRegex = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    
    if (emailRegex.test(email) === false) {
        throw Error('Email is incorrect.', { cause: 'email' });
    }

    // Do not save if blank password, no need to validate
    if (password.length === 0 && password2.length === 0) return;

    if (password.trim().length < 4) {
        throw Error('Password should be at least 4 characters long.', { cause: 'password' });
    }
    if (password !== password2) {
        throw Error('Password did not match.', { cause: 'password' });
    }
};

export const load = (async ({ params }) => {
    try {
        const user = await prisma.user.findFirstOrThrow({
            where: {
                id: parseInt(params.id)
            },
            select: {
                id: true,
                email: true,
                name: true,
                createdAt: true,
                updatedAt: true
            }
        })
        return { user };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;

export const actions = {
    save: async ({ request, params }) => {
        const formData = await request.formData();
        const hash = await pwd.hash(Buffer.from(formData.get('password')?.toString() ?? ''));
        const data = {
            email: formData.get('email')?.toString() ?? '',
            name: formData.get('name')?.toString() ?? ''
        };
        try {
            validate(formData);
            if (formData.get('password')?.toString().length === 0 && formData.get('password2')?.toString().length === 0) {
                await prisma.user.update({
                    data,
                    where: {
                        id: parseInt(params.id)
                    }
                });
            } else {
                await prisma.user.update({
                    data: {
                        ...data,
                        password: hash.toString()
                    },
                    where: {
                        id: parseInt(params.id)
                    }
                });
            }
        } catch (error) {
            if (error instanceof Error) 
                return fail(422, { error: error.message, cause: error.cause, ...data });
        }
        throw redirect(303, '/users');
    }
} satisfies Actions;