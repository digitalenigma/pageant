import type { Actions, PageServerLoad } from './$types';
import { fail, error, redirect } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const load = (async ({ params }) => {
    try {
        const user = await prisma.user.findFirstOrThrow({
            where: {
                id: parseInt(params.id)
            },
            select: {
                id: true,
                email: true,
                createdAt: true,
                updatedAt: true
            }
        })
        return { user };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;

export const actions = {
    delete: async ({ params }) => {
        try {
            await prisma.user.delete({
                where: {
                    id: parseInt(params.id)
                }
            });
        } catch (error) {
            if (error instanceof Error) 
                return fail(422, { error: error.message });
        }
        throw redirect(303, '/users');
    }
} satisfies Actions;