import type { Actions } from './$types';
import { fail } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';
import SecurePassword from 'secure-password';

const prisma = new PrismaClient();
const pwd = new SecurePassword();

const validate = (data: FormData) => {
    const email = data.get('email')?.toString() ?? '';
    const password = data.get('password')?.toString() ?? '';
    const password2 = data.get('password2')?.toString() ?? '';
    const emailRegex = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    
    if (emailRegex.test(email) === false) {
        throw Error('Email is incorrect.', { cause: 'email' });
    }
    if (password.trim().length < 4) {
        throw Error('Password should be at least 4 characters long.', { cause: 'password' });
    }
    if (password !== password2) {
        throw Error('Password did not match.', { cause: 'password' });
    }
};

export const actions = {
    save: async ({ request }) => {
        const formData = await request.formData();
        const hash = await pwd.hash(Buffer.from(formData.get('password')?.toString() ?? ''));
        const data = {
            email: formData.get('email')?.toString() ?? '',
            name: formData.get('name')?.toString() ?? '',
            password:  hash.toString(),
            createdAt: new Date()
        };
        try {
            validate(formData);
            await prisma.user.create({
                data
            });
        } catch (error) {
            if (error instanceof Error) 
                return fail(422, { error: error.message, cause: error.cause, ...data });
        }
        return {
            success: `User ${data.email} has been created.`
        };
    }
} satisfies Actions;