import { PrismaClient } from '@prisma/client';
import type { PageServerLoad } from './$types';

const prisma = new PrismaClient();

export const load = (async () => {
    const candidates = await prisma.candidate.findMany({
        orderBy: [
            { candidateNumber: 'asc' },
            { gender: 'desc' },
            { candidate: 'asc' }
        ]
    });
    return {
        candidates
    };
}) satisfies PageServerLoad;