import type { Actions } from './$types';
import { fail } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const validate = (data: FormData) => {
    const candidate = data.get('candidate')?.toString() ?? '';
    const candidateNumber = data.get('candidateNumber')?.toString() ?? '';
    const gender = data.get('gender')?.toString() ?? '';

    if (candidate.trim().length === 0) {
        throw Error('Candidate is required.', { cause: 'candidate' });
    }
    
    if (candidateNumber.trim().length === 0) {
        throw Error('Candidate Number is required.', { cause: 'candidateNumber' });
    }

    if (gender.trim() !== 'M' && gender.trim() !== 'F') {
        throw Error('Gender is should be M or F.', { cause: 'gender' });
    }
};

export const actions = {
    save: async ({ request }) => {
        const formData = await request.formData();
        const data = {
            candidate: formData.get('candidate')?.toString() ?? '',
            candidateNumber: parseInt(formData.get('candidateNumber')?.toString() ?? ''),
            gender: formData.get('gender')?.toString() ?? ''
        };
        try {
            validate(formData);
            await prisma.candidate.create({
                data
            });
        } catch (error) {
            if (error instanceof Error) 
                return fail(422, { error: error.message, cause: error.cause, ...data });
        }
        return {
            success: `Candidate ${data.candidate} has been created.`
        };
    }
} satisfies Actions;