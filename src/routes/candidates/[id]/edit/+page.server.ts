import type { Actions, PageServerLoad } from './$types';
import { error, fail, redirect } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const validate = (data: FormData) => {
    const candidate = data.get('candidate')?.toString() ?? '';
    const candidateNumber = data.get('candidateNumber')?.toString() ?? '';
    const gender = data.get('gender')?.toString() ?? '';

    if (candidate.trim().length === 0) {
        throw Error('Candidate is required.', { cause: 'candidate' });
    }
    
    if (candidateNumber.trim().length === 0) {
        throw Error('Candidate Number is required.', { cause: 'candidateNumber' });
    }

    if (gender.trim() !== 'M' && gender.trim() !== 'F') {
        throw Error('Gender is should be M or F.', { cause: 'gender' });
    }
};

export const load = (async ({ params }) => {
    try {
        const candidate = await prisma.candidate.findFirstOrThrow({
            where: {
                id: parseInt(params.id)
            }
        })
        return { candidate };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;

export const actions = {
    save: async ({ request, params }) => {
        const formData = await request.formData();
        const data = {
            candidate: formData.get('candidate')?.toString() ?? '',
            candidateNumber: parseInt(formData.get('candidateNumber')?.toString() ?? ''),
            gender: formData.get('gender')?.toString() ?? ''
        };
        try {
            validate(formData);
            await prisma.candidate.update({
                where: {
                    id: parseInt(params.id)
                },
                data
            });
        } catch (error) {
            if (error instanceof Error) 
                return fail(422, { error: error.message, cause: error.cause, ...data });
        }
        throw redirect(303, '/candidates');
    }
} satisfies Actions;