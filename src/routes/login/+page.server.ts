import type { Actions } from './$types';
import { fail, redirect } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';
import SecurePassword from 'secure-password';
import type { PageServerLoad } from './$types';

const prisma = new PrismaClient();

export const load = (async ({ cookies }) => {
    const sessionId = cookies.get('session');
    if (!sessionId) {
        return;
    }
    const userSession = await prisma.userSession.findFirst({
        where: {
            sessionId
        }
    });
    if (!userSession) {
        cookies.delete('session');
    } else {
        throw redirect(303, '/');
    }
}) satisfies PageServerLoad;

export const actions = {
    default: async ({ cookies, request }) => {
        const formData = await request.formData();
        const email = formData.get('email')?.toString() ?? '';
        const password = formData.get('password')?.toString() ?? '';
        try {
            validateForm(email, password)
            const user = await prisma.user.findFirstOrThrow({
                where: {
                    email
                }
            });

            await validatePassword(password, user.password);
            const userSession = await prisma.userSession.create({
                data: {
                    userId: user.id,
                    activeAt: new Date()
                }
            });
            cookies.set('session', userSession.sessionId);
        } catch (err) {
            if (err instanceof Error) {
                return fail(400, {
                    error: err.message,
                    cause: err.cause,
                    email,
                    password
                });
            }
        }
        throw redirect(303, '/');
    }
} satisfies Actions;

const validateForm = (email: string, password: string) => {
    if (email.trim().length === 0) {
        throw new Error('E-Mail is required.', { cause: 'email' });
    }
    if (password.trim().length === 0) {
        throw new Error('Password is required', { cause: 'password'});
    }
}

const validatePassword = async (formPassword: string, userPassword: string) => {
    const pwd = new SecurePassword();
    const pwdResult = await pwd.verify(Buffer.from(formPassword), Buffer.from(userPassword));
    if (pwdResult !== SecurePassword.VALID && pwdResult !== SecurePassword.VALID_NEEDS_REHASH) {
        throw new Error('Invalid e-mail and password.');
    }
}