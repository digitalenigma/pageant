import { PrismaClient } from '@prisma/client';
import type { PageServerLoad } from './$types';

const prisma = new PrismaClient();

export const load = (async () => {
    const categories = await prisma.category.findMany({
        orderBy: [
            { weight: 'asc' },
            { category: 'asc' }
        ]
    });
    return {
        categories
    };
}) satisfies PageServerLoad;