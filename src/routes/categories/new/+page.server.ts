import type { Actions } from './$types';
import { fail } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const validate = (data: FormData) => {
    const category = data.get('category')?.toString() ?? '';

    if (category.trim().length === 0) {
        throw Error('Category is required.', { cause: 'category' });
    }
};

export const actions = {
    save: async ({ request }) => {
        const formData = await request.formData();
        const data = {
            category: formData.get('category')?.toString() ?? '',
            weight: parseInt(formData.get('weight')?.toString() ?? '0'),
            locked: parseInt(formData.get('locked')?.toString() ?? '0') === 1
        };
        try {
            validate(formData);
            await prisma.category.create({
                data
            });
        } catch (error) {
            if (error instanceof Error) 
                return fail(422, { error: error.message, cause: error.cause, ...data });
        }
        return {
            success: `Category ${data.category} has been created.`
        };
    }
} satisfies Actions;