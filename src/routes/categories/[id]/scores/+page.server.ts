import { PrismaClient, type CandidateScore, type User, type UserSession, type Candidate, type Criterion } from '@prisma/client';
import type { PageServerLoad } from './$types';
import { error, type Actions, fail, redirect, type Cookies } from '@sveltejs/kit';

const prisma = new PrismaClient();

const getCategories = (async (id: string) => {
    return await prisma.category.findFirstOrThrow({
        where: {
            id: parseInt(id)
        },
        include: {
            criteria: {
                orderBy: [
                    { weight: 'asc' },
                    { criterion: 'asc' }
                ]
            },
            candidates: {
                orderBy: [
                    { candidateNumber: 'asc'},
                    { gender: 'desc' },
                    { candidate: 'asc' }
                ],
                include: {
                    scores: {
                        orderBy: [
                            { user: { name: 'asc' }}
                        ],
                        include: {
                            user: true
                        }
                    },
                }
            },
        }
    });
});
export const load = (async ({ params, cookies }) => {
    await validUserOrRedirect(cookies);
    try {
        const category = await getCategories(params.id);
        return { category };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;

const validUserOrRedirect = (async (cookies: Cookies) => {
    const sessionId = cookies.get('session');
    let user: UserSession & {
        user: User & {
            scores: CandidateScore[];
        };
    } | undefined;
    if (!sessionId) {
        throw redirect(303, '/logout');
    }

    try {
        user = await prisma.userSession.findFirstOrThrow({
            where: {
                sessionId
            },
            include: {
                user: {
                    include: {
                        scores: true
                    }
                }
            }
        });
    } catch (err) {
        user = undefined;
    }
    if (!user) {
        throw redirect(303, '/logout');
    }
    return user;
});
