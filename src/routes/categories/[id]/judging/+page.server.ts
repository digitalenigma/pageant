import { PrismaClient, type CandidateScore, type User, type UserSession, type Candidate, type Criterion } from '@prisma/client';
import type { PageServerLoad } from './$types';
import { error, type Actions, fail, redirect, type Cookies } from '@sveltejs/kit';

const prisma = new PrismaClient();

const getCategories = (async (id: string) => {
    return await prisma.category.findFirstOrThrow({
        where: {
            id: parseInt(id)
        },
        include: {
            criteria: {
                where: {
                    hidden: false
                },
                orderBy: [
                    { weight: 'asc' },
                    { criterion: 'asc' }
                ]
            },
            candidates: {
                orderBy: [
                    { candidateNumber: 'asc'},
                    { gender: 'desc' },
                    { candidate: 'asc' }
                ]
            },
        }
    });
});
export const load = (async ({ params, cookies }) => {
    const userSession = await validUserOrRedirect(cookies);
    try {
        const category = await getCategories(params.id);
        return { category, scores: userSession.user.scores };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;

export const actions = {
    default: async ({ request, params, cookies }) => {
        const userSession = await validUserOrRedirect(cookies);
        // actual form saving
        const formData = await request.formData();
        const category = await getCategories(params.id ?? '');
        for (const candidate of category.candidates) {
            for (const criterion of category.criteria) {
                const fieldName = `score[${candidate.id}][${criterion.id}]`;
                const fieldValue = formData.get(fieldName)?.toString() ?? '';
                if (fieldValue.length === 0) return;
                if (isNaN(parseFloat(fieldValue))) {
                    return fail(422, { error: 'Some of the fields entered is not a number.', cause: fieldName});
                } else {
                    if (criterion.percent < parseFloat(fieldValue)) {
                        console.log('error');
                        return fail(422, { error: 'The value entered is greater that the maximum score possible.' });
                    }
                    try {
                        await saveScore(candidate, userSession, criterion, fieldValue);
                    } catch (err) {
                        if (err instanceof Error)
                        return fail(422, { error: err.message });
                    }
                }
            };
        };
        return { success: 'Scores have been saved successfully.' };
    }
} satisfies Actions;

const validUserOrRedirect = (async (cookies: Cookies) => {
    const sessionId = cookies.get('session');
    let user: UserSession & {
        user: User & {
            scores: CandidateScore[];
        };
    } | undefined;
    if (!sessionId) {
        throw redirect(303, '/logout');
    }

    try {
        user = await prisma.userSession.findFirstOrThrow({
            where: {
                sessionId
            },
            include: {
                user: {
                    include: {
                        scores: true
                    }
                }
            }
        });
    } catch (err) {
        user = undefined;
    }
    if (!user) {
        throw redirect(303, '/logout');
    }
    return user;
});

const saveScore = async (candidate: Candidate, userSession: UserSession, criterion: Criterion, score: string) => {
    await prisma.candidateScore.upsert({
        where: {
            candidateId_userId_criterionId: {
                candidateId: candidate.id,
                userId: userSession?.userId ?? 0,
                criterionId: criterion.id
            }
        },
        create: {
            candidate: {
                connect: {
                    id: candidate.id
                },
            },
            user: {
                connect: {
                    id: userSession?.userId
                }
            },
            criterion: {
                connect: {
                    id: criterion.id
                }
            },
            score: parseFloat(score)
        },
        update: {
            score: parseFloat(score)
        }
    })
};