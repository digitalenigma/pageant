import type { Actions, PageServerLoad } from './$types';
import { fail, error, redirect } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const load = (async ({ params }) => {
    try {
        const category = await prisma.category.findFirstOrThrow({
            where: {
                id: parseInt(params.id)
            }
        })
        return { category };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;

export const actions = {
    delete: async ({ params }) => {
        try {
            await prisma.category.delete({
                where: {
                    id: parseInt(params.id)
                }
            });
        } catch (error) {
            if (error instanceof Error) 
                return fail(422, { error: error.message });
        }
        throw redirect(303, '/categories');
    }
} satisfies Actions;