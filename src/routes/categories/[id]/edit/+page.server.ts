import type { Actions, PageServerLoad } from './$types';
import { fail, error, redirect } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const load = (async ({ params }) => {
    try {
        const category = await prisma.category.findFirstOrThrow({
            where: {
                id: parseInt(params.id)
            }
        })
        return { category };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;

const validate = (data: FormData) => {
    const category = data.get('category')?.toString() ?? '';

    if (category.trim().length === 0) {
        throw Error('Category is required.', { cause: 'category' });
    }
};

export const actions = {
    save: async ({ request, params }) => {
        const formData = await request.formData();
        const data = {
            category: formData.get('category')?.toString() ?? '',
            weight: parseInt(formData.get('weight')?.toString() ?? '0'),
            locked: parseInt(formData.get('locked')?.toString() ?? '0') === 1
        };
        try {
            validate(formData);
            await prisma.category.update({
                where: {
                    id: parseInt(params.id)
                },
                data
            });
        } catch (error) {
            if (error instanceof Error) 
                return fail(422, { error: error.message, cause: error.cause, ...data });
        }
        throw redirect(303, '/categories');
    }
} satisfies Actions;