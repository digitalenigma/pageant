import { PrismaClient } from '@prisma/client';
import type { PageServerLoad } from './$types';
import { error, type Actions, fail, redirect } from '@sveltejs/kit';

const prisma = new PrismaClient();

export const load = (async ({ params }) => {
    try {
        const category = await prisma.category.findFirstOrThrow({
            where: {
                id: parseInt(params.id)
            },
            include: {
                criteria: {
                    orderBy: [
                        { weight: 'asc'},
                        { criterion: 'asc' }
                    ]
                },
                candidates: {
                    orderBy: [
                        { candidateNumber: 'asc' },
                        { gender: 'desc' },
                        { candidate: 'asc' }
                    ]
                }
            }
        })
        const criteria = (await prisma.criterion.findMany({
            where: {
                categories: { none: { id: parseInt(params.id) } }
            },
            orderBy: [
                { weight: 'asc'},
                { criterion: 'asc' }
            ]
        })) ?? [];
        const candidates = (await prisma.candidate.findMany({
            where: {
                categories: { none: { id: parseInt(params.id) } }
            },
            orderBy: [
                { candidateNumber: 'asc' },
                { gender: 'desc' },
                { candidate: 'asc' }
            ]
        })) ?? [];
        return { category, criteria, candidates };
    } catch (err) {
        if (err instanceof Error)
            throw error(401, err.message);
    }
}) satisfies PageServerLoad;

export const actions = {
    criteria: async ({ request, params }) => {
        try {
            const formData = await request.formData();
            if (formData.get('action') === 'add') {
                const connect = formData.getAll('availableCriteria').map((value) => ({id: parseInt(value.toString())}));
                await prisma.category.update({
                    where: {
                        id: parseInt(params.id ?? '')
                    },
                    data: {
                        criteria: { connect }
                    }
                });
            }
            if (formData.get('action') === 'remove') {
                const disconnect = formData.getAll('currentCriteria').map((value) => ({id: parseInt(value.toString())}));
                await prisma.category.update({
                    where: {
                        id: parseInt(params.id ?? '')
                    },
                    data: {
                        criteria: { disconnect }
                    }
                });
            }
        } catch (err) {
            if (err instanceof Error) {
                return fail(422, {error: err.message});
            }
        }
        throw redirect(303, `/categories/${params.id}`);
    },
    candidates: async ({ request, params }) => {
        try {
            const formData = await request.formData();
            if (formData.get('action') === 'add') {
                const connect = formData.getAll('availableCandidates').map((value) => ({id: parseInt(value.toString())}));
                await prisma.category.update({
                    where: {
                        id: parseInt(params.id ?? '')
                    },
                    data: {
                        candidates: { connect }
                    }
                });
            }
            if (formData.get('action') === 'remove') {
                const disconnect = formData.getAll('currentCandidates').map((value) => ({id: parseInt(value.toString())}));
                await prisma.category.update({
                    where: {
                        id: parseInt(params.id ?? '')
                    },
                    data: {
                        candidates: { disconnect }
                    }
                });
            }
        } catch (err) {
            if (err instanceof Error) {
                return fail(422, {error: err.message});
            }
        }
        throw redirect(303, `/categories/${params.id}`);
    },
} satisfies Actions;