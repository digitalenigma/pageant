import { redirect } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const GET = (({ url, cookies }) => {
    cookies.delete('session');
    throw redirect(303, '/');
}) satisfies RequestHandler;