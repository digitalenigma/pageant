-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Criterion" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "criterion" TEXT NOT NULL,
    "percent" REAL NOT NULL,
    "hidden" BOOLEAN NOT NULL,
    "weight" INTEGER NOT NULL DEFAULT 0
);
INSERT INTO "new_Criterion" ("criterion", "hidden", "id", "percent") SELECT "criterion", "hidden", "id", "percent" FROM "Criterion";
DROP TABLE "Criterion";
ALTER TABLE "new_Criterion" RENAME TO "Criterion";
CREATE TABLE "new_Category" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "category" TEXT NOT NULL,
    "locked" BOOLEAN NOT NULL,
    "weight" INTEGER NOT NULL DEFAULT 0
);
INSERT INTO "new_Category" ("category", "id", "locked") SELECT "category", "id", "locked" FROM "Category";
DROP TABLE "Category";
ALTER TABLE "new_Category" RENAME TO "Category";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
