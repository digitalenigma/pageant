/*
  Warnings:

  - You are about to alter the column `score` on the `CandidateScore` table. The data in that column could be lost. The data in that column will be cast from `Decimal` to `Float`.
  - You are about to alter the column `percent` on the `Criterion` table. The data in that column could be lost. The data in that column will be cast from `Decimal` to `Float`.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_CandidateScore" (
    "candidateId" INTEGER NOT NULL,
    "userId" INTEGER NOT NULL,
    "criterionId" INTEGER NOT NULL,
    "score" REAL NOT NULL,

    PRIMARY KEY ("candidateId", "userId", "criterionId"),
    CONSTRAINT "CandidateScore_candidateId_fkey" FOREIGN KEY ("candidateId") REFERENCES "Candidate" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "CandidateScore_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "CandidateScore_criterionId_fkey" FOREIGN KEY ("criterionId") REFERENCES "Criterion" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_CandidateScore" ("candidateId", "criterionId", "score", "userId") SELECT "candidateId", "criterionId", "score", "userId" FROM "CandidateScore";
DROP TABLE "CandidateScore";
ALTER TABLE "new_CandidateScore" RENAME TO "CandidateScore";
CREATE TABLE "new_Criterion" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "criterion" TEXT NOT NULL,
    "percent" REAL NOT NULL,
    "hidden" BOOLEAN NOT NULL
);
INSERT INTO "new_Criterion" ("criterion", "hidden", "id", "percent") SELECT "criterion", "hidden", "id", "percent" FROM "Criterion";
DROP TABLE "Criterion";
ALTER TABLE "new_Criterion" RENAME TO "Criterion";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
