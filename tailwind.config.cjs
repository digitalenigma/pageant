/** @type {import('tailwindcss').Config}*/
const config = {
  content: [
    "./src/**/*.{html,js,svelte,ts}",
    "./node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}",
  ],

  theme: {
    extend: {
      colors: {
        primary: { 50:'#e3f2fd',100: '#bbdefb',200:'#90caf9',300:'#64b5f6',400:'#42a5f5',500:'#42a5f5',600:'#1e88e5',700:'#1976d2',800:'#1565c0',900:'#0d47a1' }
      }
    }
  },

  darkMode: 'class',

  plugins: [
    require('flowbite/plugin')
  ]
};

module.exports = config;